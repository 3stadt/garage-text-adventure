package main

import (
	"fmt"
	"gitlab.com/3stadt/garage-text-adventure/framework"
	"os"
	"strings"
)

var player = Player{}

func main() {
	roomList, err := createRooms()
	if err != nil { // If there was an error, exit program
		fmt.Println(err)
		os.Exit(1)
	}
	roomList.start()
}

func (roomList RoomList) start() {
	currentRoom := RoomId("start")
	showRoomDesc := true

	line := framework.InitLinePrompt(".command_history")
	defer line.Close()

	for {

		if showRoomDesc == true {
			fmt.Println(roomList[currentRoom].Description())
		}

		fmt.Println("Was möchtest Du tun?")
		answer, err := line.Prompt("> ")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		words := strings.Split(answer, " ")
		action := words[0]
		params := words[1:]

		switch action {
		case "move":
			room, err := roomList.move(*roomList[currentRoom], Direction(params[0]))
			if err != nil {
				fmt.Println(err)
				break
			}
			fmt.Printf("\nDu bewegst Dich  Richtung %s.\nDu stehst nun im Raum %q.\n", params[0], room.Name)
			currentRoom = room.Id
			showRoomDesc = true
		case "take":
			itemName := strings.Join(params, " ")
			err = take(&player, &roomList[currentRoom].Inv, itemName)
			if err != nil {
				fmt.Println(err)
				break
			}
			fmt.Printf("Du hast %q an dich genommen.\n", itemName)
			showRoomDesc = false
		default:
			fmt.Println("Kein Plan was Du willst...")
			showRoomDesc = true
		}

	}

}