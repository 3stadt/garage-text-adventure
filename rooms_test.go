package main

import (
	"reflect"
	"testing"
)

func Test_createRooms(t *testing.T) {

	t.Run("number", func(t *testing.T) {
		rooms, err := createRooms()
		if err != nil {
			t.Error(err)
		}
		if len(rooms) != 6 {
			t.Errorf("createRooms() produced %d rooms, want %d", len(rooms), 6)
		}

		for _, room := range rooms {
			if reflect.DeepEqual(Room{}, room) {
				t.Error("createRooms() must not produce empty rooms")
				continue
			}
			if len(room.Name) < 1 {
				t.Error("createRooms() must not produce rooms with empty names")
				continue
			}
			if len(room.Doors) < 1 {
				t.Errorf("createRooms() room '%s' has %d doors, want at least %d", room.Name, len(room.Doors), 1)
			}
		}
	})
}
