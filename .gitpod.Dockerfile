FROM gitpod/workspace-full

USER root

RUN apt-get update && apt-get install -y zsh && rm -rf /var/lib/apt/lists/*

USER gitpod

ENV ZSH_THEME bira

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
