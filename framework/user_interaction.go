package framework

import (
	"fmt"
	"github.com/peterh/liner"
	"os"
	"strings"
)

type linerState liner.State

var histFile string

func InitLinePrompt(historyFile string) *linerState {
	histFile = historyFile
	line := liner.NewLiner()

	line.SetCtrlCAborts(true)
	line.SetMultiLineMode(true)

	if f, err := os.Open(histFile); err == nil {
		line.ReadHistory(f)
		f.Close()
	}

	return (*linerState)(line)
}

func (l *linerState) WriteHistory() {
	line := (*liner.State)(l)
	if f, err := os.Create(histFile); err != nil {
		fmt.Println("Error writing history file: ", err)
	} else {
		_, err = line.WriteHistory(f)
		if err != nil {
			fmt.Println(err.Error())
		}
		err = f.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}
}

func (l *linerState) Close() {
	l.WriteHistory()
	line := (*liner.State)(l)
	err := line.Close()
	if err != nil {
		_ = fmt.Sprintf("error closing input pipe: %s", err)
		os.Exit(1)
	}
}

func (l *linerState) Prompt(p string) (string, error) {
	line := (*liner.State)(l)
	answer, err := line.Prompt(p)
	if answer != "" {
		line.AppendHistory(answer)
	}
	return strings.ToLower(answer), err
}
