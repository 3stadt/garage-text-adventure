package main

import (
	"errors"
	"strings"
)

func take(p *Player, from *Inventory, itemName string) error {

	itemCount := len(from.Items)

	for i := 0; i < itemCount; i++ {

		currentItem := from.Items[i]
		currentItemName := strings.ToLower(currentItem.Name)

		if currentItemName == itemName {

			// Check if take action is allowed
			// ???
			if hasAction(currentItem.Actions, "take") == false {
				return errors.New("Can't take that")
			}

			// Make copy of item in player Inventory
			p.Inv.Items = append(p.Inv.Items, from.Items[i])
			// delete Item from room inventory
			from.Items = remove(from.Items, i)
			// exit function
			return nil
		}
	}

	return errors.New("Item not found in room.")
}

func hasAction(actions []Action, actionName string) bool {

	actionListLength := len(actions)

	for i := 0; i < actionListLength ; i++{
		if string(actions[i]) == actionName {
			return true
		}
	}

	return false
}

// Removes an element from a slice.
// "i" is the index of the element that gets removed.
// Just accept it for now. ;)
func remove(slice []Item, i int) []Item {
	return append(slice[:i], slice[i+1:]...)
}
