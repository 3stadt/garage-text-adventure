package main

import (
	"errors"
)

func (roomList RoomList) move(from Room, to Direction) (Room, error) {
	doorCount := len(from.Doors)

	// Use a loop for checking if the room has the desired direction.
	for i := 0; i < doorCount; i++ {
		// If so, return the connected room
		if from.Doors[i].Dir == to {
			return *roomList[from.Doors[i].Connection], nil
		}
	}

	// If not, return an error
	return Room{}, errors.New("no door found in direction")
}
