[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/3stadt/garage-text-adventure) 

# Initial commit

Shows how the basics of a text adventure work:

- Printing text
- Reading text
- Accessing variables