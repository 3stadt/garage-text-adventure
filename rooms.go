package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

// createRooms reads the content from file `rooms.json`
// and converts the content into a RoomList via `json.Unmarshal`
func createRooms() (RoomList, error) {
	rl := RoomList{}
	fileContent, err := ioutil.ReadFile("rooms.json")
	if err != nil {
		return rl, err
	}
	err = json.Unmarshal(fileContent, &rl)
	return rl, err
}

func (r *Room) Description() string {
	doorDirs := ""
	for _, d := range r.Doors {
		doorDirs = doorDirs + fmt.Sprintf(`%s, `, d.Dir)
	}
	doorDirs = strings.TrimSuffix(doorDirs, ", ")

	itemDesc := ""
	if len(r.Inv.Items) > 0 {
		itemList := ""
		for _, i := range r.Inv.Items {
			itemList = itemList + fmt.Sprintf(`%q, `, i.Name)
		}
		itemList = strings.TrimSuffix(itemList, ", ")
		itemDesc = fmt.Sprintf("\nEs gibt hier folgende Gegenstände: %s", itemList)
	}

	return fmt.Sprintf(`
Du befindest Dich im Raum %q.
Es gibt hier %d Türen.
Du siehst Türen in Richtung: %s.%s
`, r.Name, len(r.Doors), doorDirs, itemDesc)
}
