package main

type RoomList map[RoomId]*Room
type Direction string
type Action string
type RoomId string

const DN = Direction("north")
const DE = Direction("east")
const DS = Direction("south")
const DW = Direction("west")
const DD = Direction("down")
const DU = Direction("up")

const AT = Action("take")
const AE = Action("examine")

type Inventory struct {
	Items []Item `json:"item"`
}

type Door struct {
	Dir        Direction `json:"dir"`
	Connection RoomId    `json:"RoomId"`
}

type Item struct {
	Name    string    `json:"name"`
	Desc    string    `json:"desc"`
	Actions []Action  `json:"actions"`
	Inv     Inventory `json:"inv"`
}

type Room struct {
	Id    RoomId    `json:"room_id"`
	Name  string    `json:"name"`
	Doors []Door    `json:"doors"`
	Inv   Inventory `json:"inv"`
}

// Player is the representation of the user in the game world
// It's only purpose is to keep track of the items the user collected
type Player struct {
	Inv Inventory `json:"inv"`
}
