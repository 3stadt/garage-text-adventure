package main

import (
	"errors"
	"reflect"
	"testing"
)

func Test_move(t *testing.T) {

	var (
		north  Room
		east   Room
		south  Room
		west   Room
		center Room
	)

	north = Room{Id: "north", Name: "north", Doors: []Door{{DS, south.Id}}}
	east = Room{Id: "east", Name: "east", Doors: []Door{{DW, west.Id}}}
	south = Room{Id: "south", Name: "south", Doors: []Door{{DN, north.Id}}}
	west = Room{Id: "west", Name: "west", Doors: []Door{{DE, west.Id}}}
	center = Room{Id: "center", Name: "center", Doors: []Door{{DN, north.Id}, {DE, east.Id}, {DS, south.Id}, {DW, west.Id}}}

	roomList := RoomList{
		north.Id:  &north,
		east.Id:   &east,
		south.Id:  &south,
		west.Id:   &west,
		center.Id: &center,
	}

	type args struct {
		from Room
		to   Direction
	}
	tests := []struct {
		name     string
		args     args
		wantErr  error
		wantRoom Room
	}{
		{"to north", args{center, DN}, nil, north},
		{"to east", args{center, DE}, nil, east},
		{"to south", args{center, DS}, nil, south},
		{"to west", args{center, DW}, nil, west},
		{"missing door", args{north, DN}, errors.New("error"), Room{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotRoom, gotErr := roomList.move(tt.args.from, tt.args.to)
			if (gotErr != nil && tt.wantErr == nil) || (gotErr == nil && tt.wantErr != nil) {
				t.Errorf("move() error = %q, want %q", gotErr, tt.wantErr)
			}
			if !reflect.DeepEqual(gotRoom, tt.wantRoom) {
				if gotRoom.Name == "" {
					t.Errorf("move() room = %q, want %v", "empty room declaration", tt.wantRoom.Name)
				} else {
					t.Errorf("move() room = \n%#v\n, want \n%#v\n", gotRoom, tt.wantRoom)
				}
			}
		})
	}
}
